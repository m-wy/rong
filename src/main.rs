extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use gilrs::{Axis, Button, Event, Gilrs};
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;

pub struct App {
    gl: GlGraphics,
    pos_j1: (f64,f64),
    pos_ball: (f64,f64),
    d_ball: (f64,f64),
    w_size : (f64, f64),
    pad_len: f64,
    is_playing: bool,
    score: (i16, i16),
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const BACKGROUND: [f32; 4] = [0.1, 0.1, 0.1, 1.0];
        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
        const BLUE: [f32; 4] = [0.0, 0.0, 1.0, 1.0];
        self.w_size.0 = args.window_size[0];
        self.w_size.1 = args.window_size[1];
        self.pad_len = self.w_size.1 * 0.2;
        let pad = rectangle::rectangle_by_corners(0.0, 0.0, 25.0, self.pad_len);
        let ball = rectangle::centered_square(0.0, 0.0, 15.0);
        let pos_x = self.pos_j1.0;
        let pos_y = self.pos_j1.1;
        let ball_x = self.pos_ball.0;
        let ball_y = self.pos_ball.1;
        let window_x = self.w_size.0;
        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(BACKGROUND, gl);

            let transform1 = c.transform.trans(pos_x, pos_y);

            let transform2 = c.transform.trans(window_x - 25.0, pos_y);

            let transform_ball = c.transform.trans(ball_x, ball_y);
            let transform_score = c.transform.trans(window_x / 2.0, 0.0);

            rectangle(GREEN, pad, transform1, gl);
            rectangle(RED, pad, transform2, gl);
            rectangle(BLUE, ball, transform_ball, gl);
            // text(BLUE, FontSize , , cache: &mut C, transform_score, gl)
        });
    }

    fn update(&mut self, args: &UpdateArgs, press: bool, y_stick: f64) {
        //add is playing to state
        //add winning state
        //only reset if not playign
        if press {
            self.pos_ball.0 = self.w_size.0 / 2.0;
            self.pos_ball.1 = self.w_size.1 / 2.0;

            let dirs: (bool, bool) = (rand::random(), rand::random());
            if dirs.0 {
                self.d_ball.0 = self.w_size.0 / 5.0
            } else {
                self.d_ball.0 = -self.w_size.0 / 5.0
            }
            if dirs.1 {
                self.d_ball.1 = self.w_size.1 / 5.0
            } else {
                self.d_ball.1 = -self.w_size.1 / 5.0
            }
        }
        self.pos_j1.1 = self.w_size.1 * 0.4 + self.w_size.1 * 0.4 * y_stick;
        //Rebound on upper and lower bound
        if self.pos_ball.1 <= 15.0 || self.pos_ball.1 >= self.w_size.1 - 15.0 {
            self.d_ball.1 = -self.d_ball.1;
        }
        //Rebound on pad
        if (self.pos_ball.0 <= 40.0 || self.pos_ball.0 >= self.w_size.0 - 40.0)
            && (self.pos_ball.1 >= self.pos_j1.1 && self.pos_ball.1 <= self.pos_j1.1 + self.w_size.1 * 0.2)
        {
            self.d_ball.0 = -self.d_ball.0 * 1.05;
        }
        //hit the back (rebound before win is implemented)
        if self.pos_ball.0 <= 15.0 || self.pos_ball.0 >= self.w_size.0 - 15.0 {
            self.d_ball.0 = -self.d_ball.0;
        }
        self.pos_ball.0 += self.d_ball.0 * args.dt;
        self.pos_ball.1 += self.d_ball.1 * args.dt;
    }
}

fn main() {
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;
    let mut gilrs = Gilrs::new().unwrap();
    let mut active_gamepad = None;
    let size_x = 800.0;
    let size_y = 500.0;

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new("rpong", [size_x, size_y])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    for (_id, gamepad) in gilrs.gamepads() {
        println!("{} is {:?}", gamepad.name(), gamepad.power_info());
        active_gamepad = Some(_id);
    }

    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
        pos_j1: (0.0,0.0),
        pos_ball: (size_x / 2.0, size_y / 2.0),
        d_ball: (0.0, 0.0),
        w_size:(size_x, size_y),
        pad_len: 100.0,
        is_playing: true,
        score: (0, 0),
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            app.render(&args);
        }

        if let Some(args) = e.update_args() {
            if let Some(Event { id, event, time }) = gilrs.next_event() {
                active_gamepad = Some(id);
            }
            if let Some(gamepad) = active_gamepad.map(|id| gilrs.gamepad(id)) {
                let y: f64 = gamepad.value(Axis::LeftStickY).into();
                if gamepad.is_pressed(Button::South) {
                    app.update(&args, true, -y);
                }
                app.update(&args, false, -y);
            }
        }
    }
}
